;1
(defun checkAtom (x y)
	(if (null y)
		nil
		(if (listp (car y))
			(or (checkAtom x (car y)) (checkAtom x (cdr y)))
			(or (equal x (car y)) (checkAtom x (cdr y)))
		)
	)
)


;16
(defun checkNum (x)
	(if (null x)
		T
		(if (listp (car x))
			(and (checkNum (car x)) (checkNum (cdr x)))
			(and (numberp (car x)) (checkNum (cdr x)))
		)
	)
)

;1
(print '(MEMQ 'A '(S W F (A S))))
(print  (checkAtom 'A '(S W F (A S))))

(print '(MEMQ 'U '(G J (J))))
(print  (checkAtom 'U '(G J (J))))

(print '(MEMQ 'q '(S W F (((((((1)(q)))))))(A S))))
(print  (checkAtom 'q '(S W F (((((((1)(q)))))))(A S))))

;16
(print '(checkNum '(0 1 (J))))
(print (checkNum '(0 1 (J))))

(print '(checkNum '(0 1 (1))))
(print (checkNum '(0 1 (1))))

(print '(checkNum '(0 1 J)))
(print (checkNum '(0 1 J)))

(print '(checkNum '(0 1 1)))
(print (checkNum '(0 1 1)))