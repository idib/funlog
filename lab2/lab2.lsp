;19
(defun getSet (x y)
	(if (null (cdr x))
		y
		(getSet (remove (car y) (cdr x)) (cons (car x) y) )
	) 
)


;33
(defun countEl (x y)
	(if (null x)
		0
		(if (<= (car x) y)
			(+ 1 (countEl (cdr x) y))
			(countEl (cdr x) y)
		)
	)
)

(defun newarr (x y)
	(if (null y)
		nil
		(cons (countEl x (car y)) (newarr x (cdr y)))
	)
)





(setq r ())
(setq A '(a b c a b b c a))
(setq B '(1 2 3 4 5 7 9  1 2 3))
(setq C '(2 5 6))
(print 'a)
(print a)
(print 'b)
(print b)
(print 'c)
(print c)
(print 'r)
(print r)

(print '(getSet a r))
(print (getSet a r))
(print '(getSet b r))
(print (getSet b r))
(print '(newarr b c))
(print (newarr b c))